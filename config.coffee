
database = process.env.MONGOLAB_URI
shortUrl = process.env.SHORT_URL or 'trymeal.com/'
secret = process.env.SECRET or 'af8013u99j13f901f3'

if process.env.NODE_ENV == 'debug'
  database = 'mongodb://localhost/clone'
  shortUrl = 'http://localhost:3000/'

if process.env.NODE_ENV == 'test'
  database = 'mongodb://localhost/clone-test'
  shortUrl = 'localhost:3000/'

module.exports = {
  secret: secret
  database: database
  shortUrl: shortUrl
}

