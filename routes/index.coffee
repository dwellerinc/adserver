require 'newrelic'
express = require('express')
router = express.Router()
config = require '../config'
short = require 'short'
{ ShortURL, Metrics } = require 'short'
url = require 'url'
{ incrementDailyMetrics, incrementHourlyMetrics } = require '../util/metrics'
escape = require 'escape-html'
_ = require 'underscore'
compress = require 'compression'
db = require '../models/db'

short.connect(config.database)
short.connection.on 'error', (error) ->
  throw new Error(error)

router.use(compress())
router.use(express.static('public', {
  etag: true
  lastModified: true
  maxAge: 86400000 * 7
}))


getServerUrl = (request) ->
  return url.format(
    protocol: request.protocol
    host: request.get('host')
  )

router.get '/', (req, res) ->
  return res.redirect 301, 'https://truvi.co'

router.get /^((?!\.).)*$/, (req, res) ->
  hash = req.url.replace('/', '')
  if hash == 'favicon.ico'
    res.sendStatus(404)
    return

  ShortURL.findOne({hash:hash}).then (link) ->
    if not link
      return res.sendStatus(404)
    incrementDailyMetrics(link, 'impressions')
    incrementHourlyMetrics(link, 'impressions')
    Metrics.update(_id: link.totalMetrics, {$inc: {'impressions':1}}).then (result) ->
      longUrl = link.URL
      serverUrl = getServerUrl(req)
      headElements = link.headElements or []
      headElements = _.map headElements, (elt) ->
        json = elt.toJSON()
        attrText = _.reduce json.attributes, (memo, attr) ->
          value = escape(attr.value)
          memo += " #{attr.key}='#{value}'"
        , ''
        json.attributes = attrText
        json

      headAttributes = _.reduce link.headAttributes, (memo, attr) ->
        value = escape(attr.value)
        memo += "#{attr.key}='#{value}'"
      , ''

      res.render 'ad', {
        adHeadline: 'Easily support your presidential candidate'
        adText1: "Help your favorite presidential candidate by sharing content."
        adText2: "Shorten a link, post it online, and earn money for your candidate's campaign"
        adDestination: 'http://www.truvi.co'
        longUrl: longUrl
        clickUrl: serverUrl + '/click'
        destinationClickUrl: serverUrl + '/destinationClick'
        hash: hash
        headElements: headElements
        headAttributes: headAttributes
      }
  , (err) ->
    console.log err
    res.sendStatus(404)

router.post '/click', (req, res) ->
  hash = req.body.hash
  ShortURL.findOne({hash:hash}).then (link) ->
    incrementDailyMetrics(link, 'clicks')
    incrementHourlyMetrics(link, 'clicks')
    Metrics.update({_id: link.totalMetrics}, {$inc: {clicks: 1}}).then (result) ->
      res.sendStatus(200)
  , (err) ->
    console.log err
    res.sendStatus(404)

router.post '/destinationClick', (req, res) ->
  hash = req.body.hash
  ShortURL.findOne({hash:hash}).then (link) ->
    incrementDailyMetrics(link, 'destinationClicks')
    incrementHourlyMetrics(link, 'destinationClicks')
    Metrics.update({_id: link.totalMetrics}, {$inc: {destinationClicks: 1}}).then (result) ->
      res.sendStatus(200)

  , (err) ->
    console.log err
    res.sendStatus(404)

module.exports = router
