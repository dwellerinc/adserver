function recordClick(url) {
  var xhttp = new XMLHttpRequest();
  xhttp.open("POST", url, true);
  xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhttp.send("hash=" + HASH);
}

function continueToDestination(destinationUrl, destinationClickUrl) {
  recordDestinationClick(destinationClickUrl);
  window.location.href = destinationUrl;
}

function openAd(destinationUrl, clickUrl) {
  recordAdClick(clickUrl);
  var win = window.open(destinationUrl, "_blank");
  win.focus();
}

function recordDestinationClick(desintationClickUrl) {
  recordClick(desintationClickUrl);
}

function recordAdClick(adClickUrl) {
  recordClick(adClickUrl);
}


