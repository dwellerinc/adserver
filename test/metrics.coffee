app = require '../app'
request = require 'supertest-as-promised'
db = require '../models/db'
short = require 'short'
{ Metrics } = require 'short'
moment = require 'moment'
User = require '../models/user'

metrics = require '../util/metrics'

describe 'metrics', ->
  before (done) ->
    @url = 'www.google.com/' + Date.now()

    email = 'email' + Date.now()
    user = new User(
      email: email
      password: 'password'
    )

    return user.save().then =>
      User.findOne({
        email:email
      }, (err, user) =>
        @user = user
        done()
      )

  beforeEach (done) ->
    short.generate(
      URL: @url+Date.now()
      user: @user
    ).then (link) =>
      @link = link
      done()

  describe '#incrementMetrics', ->

    it 'updates user lastAdActivityDate', (done) ->
      dateBefore = Date.now()
      metrics.incrementMetrics(@link, 'clicks', 'hourlyMetrics', Date.now()).then (link) =>
        short.retrieve(link.hash).then (link) =>
          User.findOne({
            email:@user.email
          }, (err, user) ->
            expect(user.lastAdActivityDate).to.be > dateBefore
            expect(user.lastAdActivityDate).to.not.be.null
            done()
          )

    it 'creates metric that doesnt exist', ->
      metrics.incrementMetrics(@link, 'clicks', 'hourlyMetrics', Date.now()).then (link) =>
        short.retrieve(link.hash).then (link) =>
          expect(link.hourlyMetrics[0].clicks).to.equal 1
          expect(link.hourlyMetrics.length).to.equal 1
          expect(link.hourlyMetrics[0].impressions).to.equal 0

    it 'increments metric that already exists', ->
      timestamp = Date.now()
      metrics.incrementMetrics(@link, 'clicks', 'dailyMetrics', timestamp).then (link) =>
        metrics.incrementMetrics(link, 'clicks', 'dailyMetrics', timestamp).then (link) =>
          short.retrieve(link.hash).then (link) =>
            expect(link.dailyMetrics.length).to.equal 1
            expect(link.dailyMetrics[0].clicks).to.be >= 2
            expect(link.dailyMetrics[0].impressions).to.equal 0

    it 'creates two separate metrics', ->
      timestamp1 = Date.now()
      timestamp2 = timestamp1 + 1000
      metrics.incrementMetrics(@link, 'clicks', 'hourlyMetrics', timestamp1).then (link) =>
        metrics.incrementMetrics(@link, 'clicks', 'hourlyMetrics', timestamp2).then (link) =>
          short.retrieve(link.hash).then (link) =>
            expect(link.hourlyMetrics.length).to.eq 2
            expect(link.hourlyMetrics[0].clicks).to.eq 1
            expect(link.hourlyMetrics[1].clicks).to.eq 1


  describe '#incrementDailyMetrics', ->
    it 'adds a metric to dailyMetrics', ->
      metrics.incrementDailyMetrics(@link, 'destinationClicks').then (link) =>
        short.retrieve(link.hash).then (link) =>
          expect(link.dailyMetrics.length).to.equal 1
          expect(link.dailyMetrics[0].destinationClicks).to.equal 1


  describe '#incrementHourlyMetrics', ->
    it 'adds a metric to hourlyMetrics', ->
      metrics.incrementDailyMetrics(@link, 'destinationClicks').then (link) =>
        short.retrieve(link.hash).then (link) =>
          expect(link.dailyMetrics.length).to.equal 1
          expect(link.dailyMetrics[0].destinationClicks).to.equal 1











