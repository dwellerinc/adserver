app = require '../app'
request = require 'supertest'
db = require '../models/db'
short = require 'short'
{ Metrics } = require 'short'
moment = require 'moment'

describe 'ads', ->
  before ->
    @url = 'www.google.com/' + Date.now()

  describe 'serve ads', ->
    beforeEach (done) ->
      short.generate(
        URL: @url
      ).then (link) =>
        @link = link
        done()


    describe 'metrics', ->
      describe 'impressions', ->

        it 'increments the impressions by one', (done) ->
          request(app).get("/#{@link.hash}").expect(200).end (err, res) =>
            expect(res.statusCode).to.equal 200
            short.retrieve(@link.hash).then (link) =>
              expect(link.totalMetrics.impressions).to.equal 1
              expect(link.dailyMetrics[0].impressions).to.equal 1
              expect(link.hourlyMetrics[0].impressions).to.equal 1
              done()


      describe 'clicks', ->
        it 'increments the clicks by one', (done) ->
          request(app).post('/click').expect(200).send({hash: @link.hash}).end (err, res) =>
            expect(res.statusCode).to.equal 200
            short.retrieve(@link.hash).then (link) =>
              expect(link.totalMetrics.clicks).to.equal 1
              expect(link.dailyMetrics[0].clicks).to.equal 1
              expect(link.hourlyMetrics[0].clicks).to.equal 1
              done()



