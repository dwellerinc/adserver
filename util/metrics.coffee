moment = require 'moment'
{ Metrics } = require 'short'
Promise = require 'bluebird'
short = require 'short'
User = require '../models/user'

exports.incrementDailyMetrics = (link, property) ->
  startOfDay = moment().startOf('day')
  incrementMetrics(link, property, 'dailyMetrics', startOfDay)

exports.incrementHourlyMetrics = (link, property) ->
  startOfHour = moment().startOf('hour')
  incrementMetrics(link, property, 'hourlyMetrics', startOfHour)


exports.incrementMetrics = incrementMetrics = (link, property, metricsArrayName, timestamp) ->
  console.log link
  User.update(
    {_id:link.user},
    {$set:{lastAdActivityDate:moment().toDate()}}
  ).then (res) ->
    console.log res

  return Metrics.findOne({
    timestamp:timestamp,
    _id: {$in: link[metricsArrayName]}
  }).then (metric) ->
    if metric
      increment = {$inc: {}}
      increment.$inc[property] = 1
      Metrics.update({_id: metric._id}, increment).then (result) ->
        return link
    else
      metric = new Metrics({
        timestamp: timestamp
        user: link.user
        link: link
      })
      metric[property] = 1
      metric.save().then (metric) ->
        link[metricsArrayName].push(metric._id)
        link.save()



